<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Project;

class ProjectController extends Controller
{
    public function index()
    {
        $project = Project::all();



        return view('projects.index', compact('project'));
    }

    public function create()
    {
        $project = Project::all();



        return view('projects.create');
    }

    public function show(project $project)
    {
        
        
        //$project = Project::findOrFail($id);
        return view('projects.show', compact('project'));
    }

    public function edit(project $project)
    {
        //$project = Project::findOrFail($id);

        return view('projects.edit', compact('project'));
    }

    public function update(project $project)
    {
       $project->update(request(['title', 'description']));

       return redirect('/projects');
    }
    public function destroy(project $project)
    {
        //$project = Project::findOrFail($id)->delete();
        
        $project->delete();
        return redirect('/projects');
    }
    public function store()
    {
        
        request()->validate(['title'=>['required', 'min:3'],'description'=>['required', 'min:3']]);
      Project::create(request(['title', 'description']));
        return redirect('/projects');
 
    }




}
