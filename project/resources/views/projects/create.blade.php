@extends('layout')

@section('content')
    
<h1 class ="title">create</h1>


    <form method="POST"  action="/projects">
        {{ csrf_field() }}

        <div class="field">
 
                <label class="label" for="title">    title      </label>
        
                <div class="control">
                <input type="text" class="input {{ $errors->has('title') ? 'is-danger' : ''}}" name="title" placeholder="title" value="{{old('title')}}">
                </div>
             </div>

             <div class="field">
 
                    <label class="label" for="title">  Description  </label>
            
                    <div class="control">
                        <textarea  class="textarea {{ $errors->has('description') ? 'is-danger' : ''}}" name="description" placeholder="description"  >{{old('description')}}</textarea>
                    </div>
                 </div>
                 <div class="control">
                        <button type="submit" class="button is-link" >create </button>
                    </div>


                    @if ($errors->any())
                        
                    
                        <div class="notification is-danger">
                            @foreach ($errors->all() as $error)

                            <li>{{$error}}</li>
                                
                            @endforeach
                        </div>
                        @endif
    </form>
    
    
    @endsection