@extends('layout')

@section('content')
    
<h1 class ="title">Edit project</h1>


<form method="POST" action="/projects/{{$project->id}}" style='margin-bottom:1em'>
        @method('PATCH')
        @csrf
    
    <div class="field">
 
        <label class="label" for="title">    title      </label>

        <div class="control">
        <input type="text" class="input" name="title" placeholder="title" value="{{$project->title}}">
        </div>
     </div>

     <div class="field">
 
        <label class="label" for="title">  Description  </label>

        <div class="control">
            <textarea  class="textarea" name="description" > {{$project->description}}</textarea>
        </div>
     </div>

     <div class="field">
 
        <label class="label" for="title">          </label>

        <div class="control">
            <button type="submit" class="button is-link" >Update project </button>
        </div>
     </div>
    </form>
<form method="POST" action="/projects/{{$project->id}}">
        @method('DELETE')
        @csrf
     <div class="field">
 
        <label class="label" for="title">          </label>

        <div class="control">
            <button type="submit" class="button" >delete </button>
        </div>
     </div>
</form>
@endsection
