<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.2/css/bulma.min.css">
    <title>Document</title>
</head>
<body>
    <div class="container" >

        @yield('content')

    </div>
    
</body>
</html>